#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from f62 device
$(call inherit-product, device/samsung/f62/device.mk)

# Device identifier. This must come after all inclusions
PRODUCT_DEVICE := f62
PRODUCT_NAME := lineage_f62
PRODUCT_BRAND := samsung
PRODUCT_MODEL := SM-E625F
PRODUCT_MANUFACTURER := samsung

# Build info
PRODUCT_GMS_CLIENTID_BASE := android-samsung-ss

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="f62ins-user 13 TP1A.220624.014 E625FDDU2CVK2 release-keys"

BUILD_FINGERPRINT := samsung/f62ins/f62:13/TP1A.220624.014/E625FDDU2CVK2:user/release-keys
